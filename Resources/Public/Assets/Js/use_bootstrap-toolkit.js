var teufels_thm_bs_toolkit__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        if (typeof teufels_thm_bs__loaded == "boolean" && teufels_thm_bs__loaded
                && typeof teufels_cfg_typoscript__windowLoad == "boolean" && teufels_cfg_typoscript__windowLoad
                && typeof ResponsiveBootstrapToolkit == 'object') {

            clearInterval(teufels_thm_bs_toolkit__interval);
            if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
                console.info('jQuery - Bootstrap Toolkit loaded');
            }

        }

    }

}, 2000);