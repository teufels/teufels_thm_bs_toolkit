if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
    var teufels_thm_bs_toolkit_example__interval = setInterval(function () {

        if (typeof jQuery == 'undefined') {
        }  else {

            if (typeof teufels_thm_bs__loaded == "boolean" && teufels_thm_bs__loaded
                && typeof teufels_cfg_typoscript__windowLoad == "boolean" && teufels_cfg_typoscript__windowLoad
                && typeof ResponsiveBootstrapToolkit == 'object') {

                clearInterval(teufels_thm_bs_toolkit_example__interval);
                console.info('jQuery - Bootstrap Toolkit example loaded');


                (function($, document, window, viewport){

                    console.log('Current breakpoint:', viewport.current());

                    $(window).resize(
                        viewport.changed(function(){
                            console.log('Current breakpoint:', viewport.current());
                        })
                    );

                })(jQuery, document, window, ResponsiveBootstrapToolkit);

            }

        }

    }, 2000);
}