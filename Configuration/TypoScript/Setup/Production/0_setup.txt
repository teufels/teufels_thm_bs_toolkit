################################
## SETUP BOOTSTRAP-TOOLKIT.JS ##
################################

[globalVar = LIT:1 = {$plugin.tx_teufels_thm_bs_toolkit.settings.lib.bsToolkit.bUseBsToolkit}]
    page {
        includeJSFooterlibs  {

            teufels_thm_bs_toolkit__js = {$plugin.tx_teufels_thm_bs_toolkit.settings.production.includePath.public}Assets/Js/bootstrap-toolkit.min.js
            teufels_thm_bs_toolkit__js.async = 1

            teufels_thm_bs_toolkit__use_js = {$plugin.tx_teufels_thm_bs_toolkit.settings.production.includePath.public}Assets/Js/use_bootstrap-toolkit.min.js
            teufels_thm_bs_toolkit__use_js.async = 1

            teufels_thm_bs_toolkit__example_js = {$plugin.tx_teufels_thm_bs_toolkit.settings.production.includePath.public}Assets/Js/basic_use.min.js
            teufels_thm_bs_toolkit__example_js.async = 1
        }
    }
[global]